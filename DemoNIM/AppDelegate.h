//
//  AppDelegate.h
//  DemoNIM
//
//  Created by chenzm on 2018/8/20.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

