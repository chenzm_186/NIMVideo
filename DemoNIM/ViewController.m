//
//  ViewController.m
//  DemoNIM
//
//  Created by chenzm on 2018/8/20.
//  Copyright © 2018年 chenzm. All rights reserved.
//

#import "ViewController.h"
#import "VideoVC.h"

#import <NIMSDK/NIMSDK.h>
#import <NIMAVChat/NIMAVChat.h>


@interface ViewController ()

///按钮
@property(nonatomic,strong)UIButton *btn;


@end

@implementation ViewController

#pragma mark - initial
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBaseUI];
}

-(void)setBaseUI{
    self.view.backgroundColor = [UIColor yellowColor];
    [self btn];
}


#pragma mark - method
-(void)btnAction:(UIButton *)sender{
    //房间id【服务器端创建】
    NSString *roomId = @"xxx";
    __weak typeof(self) weaself = self;
    [self getNimLoginWithShowHud:NO response:^(NSError *error) {
        [weaself zm_enterChatroomWithRoomId:roomId response:^(NSError * _Nullable error, NIMChatroom * _Nullable chatroom, NIMChatroomMember * _Nullable me) {
            [weaself jumpToVideoVC];
        }];
    }];
}

///跳转页面
-(void)jumpToVideoVC{
    VideoVC *vc = [VideoVC new];
    [self presentViewController:vc animated:YES completion:^{}];
}


///云信登录请求
- (void)getNimLoginWithShowHud:(BOOL)isShowHudErr response:(void(^)(NSError *error))response{
    //云信登录
    NSString *user_accid =  @"xxx";
    NSString *user_nimToken = @"xxx";
    
    [[[NIMSDK sharedSDK] loginManager] login:user_accid token:user_nimToken completion:^(NSError *error) {
        !response?:response(error);
        if (error == nil){
            NSString *userID = [NIMSDK sharedSDK].loginManager.currentAccount;
            if (userID) {
                
                NSString *nick = @"昵称";
                NSString *headerUrl = @"https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLGrrnBlrnGDwlX6LOIhDv1UMicXKJ9jAv9TVicbVRmI6es3SkXU7bQrxziawS1LoR7CM54BNbrNTphg/132";
                NSDictionary *dic = @{@(NIMUserInfoUpdateTagNick):nick,@(NIMUserInfoUpdateTagAvatar):headerUrl};
                //更新昵称头像信息
                [[NIMSDK sharedSDK].userManager updateMyUserInfo:dic completion:^(NSError *error) {
                    !response?:response(error);
                }];
                NSLog(@"云信登录成功");
            }
        }else{
            if (isShowHudErr == YES) {
                NSString *err = [NSString stringWithFormat:@"登录失败:%ld",(long)error.code];
                NSLog(@"%@",err);
            }
        }
    }];
}


/**
 进入聊天室
 @param roomId 聊天室id
 @param completion 返回结果
 */
-(void)zm_enterChatroomWithRoomId:(NSString *_Nullable)roomId response:(nullable NIMChatroomEnterHandler)completion{
    
    NIMUser *user = [[NIMSDK sharedSDK].userManager userInfo:[NIMSDK sharedSDK].loginManager.currentAccount];
    NIMChatroomEnterRequest *request = [[NIMChatroomEnterRequest alloc] init];
    request.roomId = roomId;
    request.roomNickname = user.userInfo.nickName;
    request.roomAvatar = user.userInfo.avatarUrl;
    
    [[[NIMSDK sharedSDK] chatroomManager] enterChatroom:request completion:^(NSError * _Nullable error, NIMChatroom * _Nullable chatroom, NIMChatroomMember * _Nullable me) {
        !completion?:completion(error,chatroom,me);
        if (error == nil){
            
        }else{
            NSLog(@"进入失败 enter room %@ failed %@ code:%zd",chatroom.roomId,error,error.code);
        }
        
    }];
    
}

#pragma mark - lazyload
-(UIButton *)btn{
    if (!_btn) {
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn.frame = CGRectMake(100, 100, 100, 100);
        _btn.backgroundColor = [UIColor redColor];
        [_btn setTitle:@"点击跳转" forState:UIControlStateNormal];
        [_btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_btn];
    }
    return _btn;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
